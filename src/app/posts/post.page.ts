import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {LoadingController, Platform} from '@ionic/angular';
import { WordPressRestapiService } from '../services/wordpress-restapi.service';
import { Post, Media, Tag, Category, Comment, User } from '../services/classes.service';


@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss'],
})
export class PostPage implements OnInit {

  id: string;
  private post: Post = new Post;
  private media: Media = new Media;
  private tags: Tag[] = [];
  private categories: Category[] = [];
  private comments: Comment[] = [];
  private users : User[] = [];

  constructor(
      public loadingController: LoadingController,
      private route: ActivatedRoute,
      public plt : Platform,
      private wordpressService: WordPressRestapiService) { }

  async ngOnInit() {

    const loading = await this.loadingController.create();
    await loading.present();

    this.id = this.route.snapshot.paramMap.get('id');

    this.getPost(this.id).subscribe((data: any) => {
      this.post = data[0];
      this.comments = data[1];
      this.media = data[2];
      this.tags = data[3];
      this.categories = data[4];
      this.users = data[5];
      this.ShowHidden();
      loading.dismiss();
        console.log(this.post)
    });


  }

  getPost(postId) {
    return this.wordpressService.getPost(postId);
  }

  isMobile(){
    return this.plt.width() <= 1024;
  }

  ShowHidden(){
    document.getElementById("ShowContainer").style.display ="flex";
    document.getElementById("ShowFooter").style.display ="block";
    document.getElementById("ShowToolbar").style.display ="block";
    document.getElementById("ShowUpperBorder").style.display ="block";
  }
}
