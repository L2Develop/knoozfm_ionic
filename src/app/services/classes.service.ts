import { Injectable } from '@angular/core';
import {formatDate} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ClassesService {

  constructor() { }
}

export class Post {
  author: number;
  categories: number[];
  comment_status: string;
  content: object;
  date: string;
  date_gmt: string;
  excerpt: object;
  featured_media: number;
  format: string;
  guid: object;
  id: number;
  link: string;
  media: object;
  meta: object;
  modified: string;
  modified_gmt: string;
  ping_status: string;
  slug: string;
  status: string;
  sticky: boolean;
  tags: number[];
  template: string;
  title: object;
  type: string;
  _links: object;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

  getTitle() {
    if (this.title.hasOwnProperty('rendered')) {
      return this.title['rendered'];
    }
    return this.title;
  }

  getDate() {
    return formatDate(this.date, 'mediumDate', 'en-GB');
  }
}

export class Media {
  date: string;
  date_gmt: string;
  guid: object;
  id: number;
  link: string;
  modified: string;
  modified_gmt: string;
  slug: string;
  status: string;
  type: string;
  title: object;
  author: number;
  comment_status: string;
  ping_status: string;
  meta: object;
  template: string;
  alt_text: string;
  caption: object;
  description: object;
  media_type: string;
  mime_type: string;
  media_details: object;
  post: number;
  source_url: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

  hasThumbnail() {
    return Object.keys(this).length > 0;
  }

  getThumbnail(size: string = 'thumbnail') {
    if (this.media_details.hasOwnProperty('sizes')) {
      if (size == 'full')
        return this.media_details['sizes'].full.source_url;

      if (size == 'medium')
        return this.media_details['sizes'].medium.source_url;

      if (size == 'slider')
        return this.media_details['sizes'].slider.source_url;

      return this.media_details['sizes'].thumbnail.source_url;
    }

    return;
  }


}

export class Tag {
  id: number;
  count: number;
  description: string;
  link: string;
  name: string;
  slug: string;
  taxonomy: string;
  meta: object;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class Category {
  id: number;
  count: number;
  description: string;
  link: string;
  name: string;
  slug: string;
  taxonomy: string;
  parent: number;
  meta: object;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class Comment {
  id: number;
  author: number;
  author_email: string;
  author_ip: string;
  author_name: string;
  author_url: string;
  author_user_agent: string;
  content: object;
  date: string;
  date_gmt: string;
  link: string;
  parent: number;
  post: number;
  status: string;
  type: string;
  author_avatar_urls: object;
  meta: object;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class User {
  id: number;
  name: string;
  url: string;
  link: string;
  meta: object;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
