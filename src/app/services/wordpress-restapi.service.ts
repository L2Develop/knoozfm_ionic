import { Injectable } from '@angular/core';
import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, throwError, empty, of } from 'rxjs';
import { catchError, map, mergeMap, flatMap } from 'rxjs/operators';
import { Category,Comment,Media,Post,Tag,User } from './classes.service';

@Injectable({
  providedIn: 'root'
})
export class WordPressRestapiService {

    page = 1;
  baseRestApiUrl: string = "https://news.ghanamotion.com//wp-json/wp/v2/";

  constructor(private httpClient: HttpClient) { }

  getRecentPosts(categoryId: number, page : number = 1): Observable<any> {
    // Get posts by a category if a category id is passed
    let category_url = categoryId ? ("&categories=" + categoryId) : "";
    return this.httpClient.get(this.baseRestApiUrl + "posts?page=" + page + category_url).pipe(
        map((res: any) => res),
        mergeMap((posts: Post[]) => {
          if (posts.length > 0) {
            return forkJoin(
                posts.map((post: Post) => {
                  if (post.featured_media === 0) {
                    post.media = new Media;
                    return of(new Post(post));
                  }
                  else {
                    return this.httpClient.get(this.baseRestApiUrl + "media/" + post.featured_media).pipe(
                        map((res: any) => {
                          post.media = new Media(res);
                          return new Post(post);
                        }),
                        catchError(val => of(val))
                    );
                  }
                })
            );
          }
          return empty();
        }),
        catchError(val => of(val))
    );
  }
  getPost(postId) {
    return this.httpClient.get(this.baseRestApiUrl + "posts/" + postId).pipe(
        map((res: any) => res),
        flatMap((post: any) => {
          return forkJoin(
              of(new Post(post)),
              this.getComments(post.id),
              this.getMedia(post.featured_media),
              this.getTags(post),
              this.getCategories(post),
              this.getUser(post.author)
          )
        })
    );
  }
  // getPost(postId: number): Observable<Post> {
  //   return this.httpClient.get(this.baseRestApiUrl + "posts/" + postId).pipe(
  //     map(post => {
  //       return new Post(post);
  //     }),
  //     catchError(val => of(val))
  //   );
  //
  getTags(post) {
    let observableBatch = [];

    post.tags.forEach(tag => {
      observableBatch.push(this.getTag(tag));
    });

    return forkJoin(observableBatch);
  }
  getTag(tagId: number): Observable<Tag> {
    return this.httpClient.get(this.baseRestApiUrl + "tags/" + tagId).pipe(
        map(tag => {
          return new Tag(tag);
        }),
        catchError(val => of(val))
    );
  }
  getUser(userId: number): Observable<User> {
    return this.httpClient.get(this.baseRestApiUrl + "users/" + userId).pipe(
        map(user => {
          return new User(user);
        }),
        catchError(val => of(val))
    );
  }
  getComments(postId: number, page: number = 1) {
    return this.httpClient.get(this.baseRestApiUrl + "comments?post=" + postId).pipe(
        map(comments => {
          let commentsArray = [];

          Object.keys(comments).forEach(function (key) {
            commentsArray.push(new Comment(comments[key]));
          });

          return commentsArray;
        }),
        catchError(val => of(val))
    );
  }
  getCategories(post) {
    let observableBatch = [];

    post.categories.forEach(category => {
      observableBatch.push(this.getCategory(category));
    });

    return forkJoin(observableBatch);
  }
  getCategory(categoryid: number): Observable<Category> {
    return this.httpClient.get(this.baseRestApiUrl + "categories/" + categoryid).pipe(
        map(category => {
          return new Category(category);
        }),
        catchError(val => of(val))
    );
  }
  getMedia(mediaId: number): Observable<Media> {
    if (mediaId > 0) {
      return this.httpClient.get(this.baseRestApiUrl + "media/" + mediaId).pipe(
          map(media => {
            return new Media(media);
          }),
          catchError(val => of(val))
      );
    }
    return of(new Media);
  }

    LoadAllcategory() {
        return this.httpClient.get(this.baseRestApiUrl + "categories/").pipe(
            map(category => {
                let categroArray = [];
                Object.keys(category).forEach(function (key) {
                    categroArray.push(new Category(category[key]));
                });
                return categroArray;
            }),
            catchError(val => of(val))
        );
    }
}


