import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import {SwiperModule} from 'ngx-swiper-wrapper';
import { SharedPipeModule } from '../pipe/sharePipe.module';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedPipeModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomePage
            }
        ]),
        SwiperModule
    ],
  declarations: [HomePage]
})
export class HomePageModule {}
