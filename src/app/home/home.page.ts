import { Component,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { WordPressRestapiService } from '../services/wordpress-restapi.service';
import {Post} from '../services/classes.service';
import { Platform,IonSlides } from '@ionic/angular';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild('mySlider',{static : false}) slides: IonSlides;
  categoryId: number;
  categories:any;
  pagination: number = 1;
  autoplay : boolean;
  page: number=0;
  private posts : Post[] = [];
  private slideOpts = {
    initialSlide: 0,
    speed: 900,
    autoplay : 0,
    parallax: true,
    breakpoints: {
  // when window width is <= 320px

  700: {
    slidesPerView: 1,
    spaceBetween: 20,
  },

  1024: {
     slidesPerView: 2,
     spaceBetween: 0,
  },
  3000: {
    slidesPerView: 4,
    spaceBetween: 2,
  }
}
  };

  constructor(
      public loadingController: LoadingController,
      private router: Router,
      private wordpressService: WordPressRestapiService,
      public plt : Platform)
  {  }

  async ngOnInit() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.loadCategories();

    this.loadPosts(this.page).subscribe((posts: Post[]) => {
      console.log(posts);
      this.posts = posts;
      loading.dismiss();
      this.ShowPic();
    });


    console.log(this.plt.width());

  }

  loadPosts(page:number) {
    return this.wordpressService.getRecentPosts(this.categoryId,page);
  }

  getMorePosts(evt) {
    this.page++;
    this.loadPosts(this.page).subscribe((posts: Post[]) => {
      for(let item of posts)
      {
        this.posts.push(item);
      }
    });
    console.log(this.posts);
  }



  openPost(postId) {
    this.router.navigateByUrl('/post/' + postId);
  }


  swipeNext() {
      this.slides.slideNext();
  }

  swipePrev(){
    this.slides.slidePrev();
  }

  isMobile(){
    return this.plt.width() <= 1024;
  }

  ShowPic(){
    document.getElementById("img-logo").style.display="block";
    document.getElementById("SlideLoader").style.display="inline-block";
    document.getElementById("ionSlideLoader").style.display="flex";
    if (this.isMobile() == true)
    document.getElementById("ButtonNext").style.display="block";
  }


  // infiniteScroll(ev){
  //   this.pagination += 1;
  //   this.wordpressService.getRecentPosts(this.pagination,this.categoryId)
  //       .toPromise()
  //       .then(data => {
  //         for(let i of data){
  //           this.posts.push(i);
  //         }
  //         ev.complete();
  //       });
  // };

  public progress = 0;
  public interval ;
  StartChanging() {
    if (this.isMobile() == true)
    {

      if (this.slides.ionSlideDidChange)
      {
        this.progress = -0.2;
        clearInterval(this.interval);
        this.interval = setInterval(()=> {
          this.progress = this.progress + 0.01;
        },23)
      }

      setTimeout(()=>{this.slides.startAutoplay()},500);
      this.slideOpts.autoplay = 5000;

    }
  }

  loadCategories(){
    return this.wordpressService.LoadAllcategory().subscribe((data: any) => {
      let categoryArray = {};

      data.map(item=>{
        categoryArray[item.id] = item.name;
        this.categories=categoryArray;
      } );
          console.log(this.categories);
      catchError(val => of(val))


    });

  }

}
