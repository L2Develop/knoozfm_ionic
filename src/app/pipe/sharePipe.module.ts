import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { CategoryIdToNamePipe } from './category-id-to-name.pipe';


@NgModule({
    declarations: [CategoryIdToNamePipe],
    imports: [
        CommonModule
    ],
    exports: [CategoryIdToNamePipe]
})
export class SharedPipeModule {}