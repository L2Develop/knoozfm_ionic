import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categoryIdToName'
})
export class CategoryIdToNamePipe implements PipeTransform {

  transform(value, args) {
    let output = '';
    // value = value + ''; // make sure it's a string
    // return value.toLowerCase();
    value.forEach(function(number){
      output = output + ' - '+ args[number]
    });
    return output;
  }

}
